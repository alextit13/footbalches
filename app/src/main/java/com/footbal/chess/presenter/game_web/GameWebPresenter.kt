package com.footbal.chess.presenter.game_web

import com.footbal.chess.model.data.ITimerCallback
import com.footbal.chess.model.data.system.IDataGameWebMovingCallback
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.addPlayersToMap
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.changeTypeOfPlayer
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.checkIsGoal
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.clearList
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.countGoalMe
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.countGoalOther
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.getBall
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.getDataForRecycler
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.getPositionSelectPlayer
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.isUserTryPass
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.listMap
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.tryPickTheBall
import com.footbal.chess.model.data.web.WebPlayerManager.isMoveUser
import com.footbal.chess.model.data.web.WebPlayerManager.movingFromSystem
import com.footbal.chess.model.util.Player
import com.footbal.chess.view.game_web.IGameWebActivity
import java.util.*

class GameWebPresenter : IGameWebPresenter, IDataGameWebMovingCallback, ITimerCallback {
    override fun timeFirstUserChange(color: Int, time: String) {

    }

    override fun timeSecondUserChange(color: Int, time: String) {

    }

    private var gameView: IGameWebActivity? = null
    private var isGameWithSystem = true

    override fun attachView(gameActivity: IGameWebActivity) {
        this.gameView = gameActivity
        this.gameView?.initViews()
        this.gameView?.initListeners()
    }

    override fun onHasFocuse(hasFocus: Boolean, width: Int, height: Int) {
        if (hasFocus)
            gameView?.initRecyclerView(getDataForRecycler(), calculateItemWidth(width), calculateItemHeight(height))
        addPlayersToMap()
        startTimer()
    }

    private fun startTimer() {

    }

    override fun onClickOnTimer() {
        isMoveUser = !isMoveUser
        if (!isMoveUser)
            movingFromSystem(this)
    }

    override fun systemPlayingFinish() {
        gameView?.showToast("Ваш ход!")
    }

    override fun onItemClick(position: Int) {
        if (isUserTryPass(position)) {
            gameView?.notifyDataInAdapter()
        } else {
            val currentPlayer = listMap[position]
            if (currentPlayer.whatIs == Player.WhatIs.PLAYER_MY || currentPlayer.whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL)
                changeSelectionPlayer(position)
            if (currentPlayer.whatIs == Player.WhatIs.CELL)
                if (isMoveUser)
                    movingPlayerFromUser(position)
                else
                    gameView?.showToast("Ход соперника")
        }
    }

    private fun changeSelectionPlayer(position: Int) {
        listMap.forEach {
            it.isSelection = false
        }
        listMap[position].isSelection = true
        gameView?.maximazePlayer(position)
    }

    private fun movingPlayerFromUser(clickPosition: Int) {
        val selectPlayerPosition = getPositionSelectPlayer()
        if (clickPositionForMovingIsCorrect(clickPosition, selectPlayerPosition)) {
            if (clickPosition in POSITION_OTHER_GOALS) {
                listMap[clickPosition] = getBall(clickPosition)
                changeTypeOfPlayer()
            } else
                Collections.swap(listMap, selectPlayerPosition, clickPosition)
            gameView?.refreshAdapter(selectPlayerPosition, clickPosition)
            tryPickTheBall(this)
            checkIsGoal(this)
        }
    }

    private fun clickPositionForMovingIsCorrect(clickPosition: Int, selectedPlayerPosition: Int): Boolean {
        return clickPosition + 1 == selectedPlayerPosition || clickPosition - 1 == selectedPlayerPosition
                || clickPosition + WIDTH_FIELD == selectedPlayerPosition || clickPosition - WIDTH_FIELD == selectedPlayerPosition
                || clickPosition + WIDTH_FIELD + 1 == selectedPlayerPosition || clickPosition + WIDTH_FIELD - 1 == selectedPlayerPosition
                || clickPosition - WIDTH_FIELD + 1 == selectedPlayerPosition || clickPosition - WIDTH_FIELD - 1 == selectedPlayerPosition
    }

    override fun movingPlayerFromSystem(from: Int, to: Int) {
        Collections.swap(listMap, from, to)
        gameView?.refreshAdapter(from, to)
        tryPickTheBall(this)
        checkIsGoal(this)
    }

    override fun refreshData() {
        gameView?.notifyDataInAdapter()
    }

    override fun onGoal(forWho: Int) {
        if (forWho == 1) countGoalMe += 1 else countGoalOther += 1
        gameView?.showToast(if (forWho == 1) "ГОЛ СОПЕРНИКУ" else "ГОЛ МНЕ")
        Thread(Runnable {
            Thread.sleep(GOAL_DELAY)
            refreshMapAndData()
        }).start()
    }

    private fun refreshMapAndData() {
        clearList()
        gameView?.refreshViews()
    }

    private fun calculateItemWidth(widthMap: Int) = widthMap / WIDTH_FIELD

    private fun calculateItemHeight(heightMap: Int) = heightMap / HEIGHT_FIELD

    override fun detachView() {
        gameView = null
    }

    override fun onTimeFinish() {
        gameView?.showFinishGameResult("$countGoalMe : $countGoalOther")
    }

    companion object {
        const val WIDTH_FIELD = 9
        const val HEIGHT_FIELD = 12
        const val NUM_OF_MY_PLAYERS_ON_START = 4
        const val NUM_OF_OTHER_PLAYERS_ON_START = 4
        const val POSITION_OF_MY_GOALKEEPER = 103
        const val POSITION_OF_OTHER_GOALKEEPER = 4
        val POSITION_OTHER_GOALS = 3..5
        val POSITION_MY_GOALS = 103..105
        val defaultPositionPlayers: MutableList<Int> = mutableListOf(4, 20, 24, 40, 67, 83, 87, 103)
        const val GOAL_DELAY = 3000L
    }
}