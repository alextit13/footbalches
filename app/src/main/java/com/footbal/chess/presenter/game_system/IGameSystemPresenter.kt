package com.footbal.chess.presenter.game_system

import com.footbal.chess.view.game_system.IGameSystemActivity

interface IGameSystemPresenter {
    fun attachView(gameActivity: IGameSystemActivity)
    fun detachView()
    fun onHasFocuse(width: Int, height: Int)
    fun onItemClick(position: Int)
    fun onClickOnTimer()
    fun onClickReturn()
    fun onPrepare(isPrepared: Boolean)
}