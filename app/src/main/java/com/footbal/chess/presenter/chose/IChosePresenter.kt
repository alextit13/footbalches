package com.footbal.chess.presenter.chose

import com.footbal.chess.model.util.Gamer
import com.footbal.chess.view.multiplayer.fragment.IChoseView

interface IChosePresenter {
    fun attachView(view: IChoseView)
    fun detachView()
    fun onClickRefresh()
    fun onClickOnGamer(gamer: Gamer)
}