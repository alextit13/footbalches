package com.footbal.chess.presenter.two_players

import com.footbal.chess.model.data.ITimerCallback
import com.footbal.chess.model.data.TimerManager.goTimerFirstPlayer
import com.footbal.chess.model.data.TimerManager.goTimerSecondPlayer
import com.footbal.chess.model.data.TimerManager.stopTimerFirstPlayer
import com.footbal.chess.model.data.TimerManager.stopTimerSecondPlayer
import com.footbal.chess.model.data.two_player.DataGameTwoPlayersManager
import com.footbal.chess.model.data.two_player.IDataGameTwoPlayerMovingCallback
import com.footbal.chess.model.util.Player
import com.footbal.chess.view.two_players.ITwoPlayerView
import java.util.*

class TwoPlayersPresenter : ITwoPlayersPresenter, IDataGameTwoPlayerMovingCallback, ITimerCallback {

    private var gameView: ITwoPlayerView? = null
    private val manager: DataGameTwoPlayersManager = DataGameTwoPlayersManager.getActualIntance()
    private var goalkeeperWasMoving = false
    private var isPrepared: Boolean = false
    private var incrementalShowsBot = 0

    override fun attachView(gameActivity: ITwoPlayerView) {
        this.gameView = gameActivity
        this.gameView?.initViews()
        this.gameView?.initListeners()
    }

    override fun onViewPrepared(isPrepared: Boolean) {
        this.isPrepared = isPrepared
    }

    override fun onHasFocuse(width: Int, height: Int) {
        if (isPrepared) {
            gameView?.initRecyclerView(
                manager.getDataForRecycler(),
                calculateItemWidth(width),
                calculateItemHeight(height)
            )
            manager.addPlayersToMap()
            startTimer()
        }
    }

    private fun startTimer() {
        if (manager.whoManage == 1) {
            stopTimerSecondPlayer()
            goTimerFirstPlayer(this)
        } else if (manager.whoManage == 2) {
            stopTimerFirstPlayer()
            goTimerSecondPlayer(this)
        }
    }

    private fun calculateItemWidth(widthMap: Int) = widthMap / WIDTH_FIELD

    private fun calculateItemHeight(heightMap: Int) = heightMap / HEIGHT_FIELD

    override fun onClickOnTimer() {
        incrementalShowsBot = 0
        manager.removeAllSelectPlayers()
        manager.whoManage = if (manager.whoManage == 1) 2 else 1
        manager.listHistory.clear()
        goalkeeperWasMoving = false
        gameView?.showGoalkeeperBot()
        makeActiveAllBots()

        if (manager.whoManage == 1) {
            stopTimerSecondPlayer()
            goTimerFirstPlayer(this)
        } else if (manager.whoManage == 2) {
            stopTimerFirstPlayer()
            goTimerSecondPlayer(this)
        }
        gameView?.showToast("Ход другого игрока")
    }

    override fun timeFirstUserChange(color: Int, time: String) {
        gameView?.setTimeInTextViewTime(color, time)
    }

    override fun timeSecondUserChange(color: Int, time: String) {
        gameView?.setTimeInTextViewTime(color, time)
    }

    private fun makeActiveAllBots() {
        for (i in 1..4) {
            gameView?.showOneBot(i)
        }
    }

    override fun onTimeFinish() {
        gameView?.showFinishGameResult(
            "${manager.countGoalMe} " +
                    ": ${manager.countGoalOther}"
        )
    }

    override fun movingPlayerFromOtherPlayer(from: Int, to: Int) {
        manager.whoManage = 2
    }

    override fun onGoal(forWho: Int) {
        if (forWho == 1)
            manager.countGoalMe = manager.countGoalMe.plus(1)
        else manager.countGoalOther =
            manager.countGoalOther.plus(1)
        gameView?.showToast(if (forWho == 1) "ГОЛ СОПЕРНИКУ" else "ГОЛ МНЕ")
        Thread(Runnable {
            Thread.sleep(GOAL_DELAY)
            refreshMapAndData()
            gameView?.recreateGame()
        }).start()
        manager.listHistory.clear()
        makeActiveAllBots()
    }

    private fun refreshMapAndData() {
        manager.clearList()
    }

    override fun refreshData(isTryWasSuccess: Boolean) {
        if (isTryWasSuccess)
            addMoveToHistory(false)
        gameView?.notifyDataInAdapter()
    }

    override fun detachView() {
        isPrepared = false
        gameView = null
    }

    override fun onItemClick(position: Int) {
        if (manager.listHistory.size < 3 || tryMovingGoalkeeper(position)) {
            tryPassToMyPlayer(position)
            changeSelectPlayer(position)
            movingPlayerFromUser(position)
        } else {
            gameView?.showToast("Ходы закончились")
        }
    }

    private fun tryMovingGoalkeeper(clickPosition: Int): Boolean {
        var result = false
        val clickOnGoalkeeper =
            manager.listMap[clickPosition].whatIs == Player.WhatIs.GOALKEEPER_MY ||
                    manager.listMap[clickPosition].whatIs == Player.WhatIs.GOALKEEPER_OTHER
        val selectionPlayer = manager.getPositionSelectPlayer()
        val selectionGoalkeeper =
            manager.listMap[selectionPlayer].whatIs == Player.WhatIs.GOALKEEPER_MY ||
                    manager.listMap[selectionPlayer].whatIs == Player.WhatIs.GOALKEEPER_OTHER

        if (clickOnGoalkeeper || selectionGoalkeeper && !goalkeeperWasMoving) {
            result = true
        }
        return result
    }

    private fun changeSelectPlayer(position: Int) {
        if (checkChangeSelectMyPlayer(position)) {
            manager.listMap[position].isSelection = true
        }
    }

    private fun checkChangeSelectMyPlayer(position: Int): Boolean {
        var isChangePlayerSelectionOnly = false
        if (position != -1) {
            if (manager.whoManage == 1) {
                if (manager.listMap[position].whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL ||
                    manager.listMap[position].whatIs == Player.WhatIs.PLAYER_MY ||
                    manager.listMap[position].whatIs == Player.WhatIs.GOALKEEPER_MY
                ) {
                    isChangePlayerSelectionOnly = true
                    manager.removeAllSelectPlayers()
                }
            } else if (manager.whoManage == 2) {
                if (manager.listMap[position].whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL ||
                    manager.listMap[position].whatIs == Player.WhatIs.PLAYER_OTHER ||
                    manager.listMap[position].whatIs == Player.WhatIs.GOALKEEPER_OTHER
                ) {
                    isChangePlayerSelectionOnly = true
                    manager.removeAllSelectPlayers()
                }
            }
        }
        return isChangePlayerSelectionOnly
    }

    override fun onClickPickBall() {
        manager.tryPickTheBall(this)
    }

    private fun movingPlayerFromUser(clickPosition: Int) {
        val selectPlayerPosition = manager.getPositionSelectPlayer()
        if (clickPositionForMovingIsCorrect(clickPosition, selectPlayerPosition)) {
            if (manager.listMap[selectPlayerPosition].whatIs == Player.WhatIs.GOALKEEPER_MY ||
                manager.listMap[selectPlayerPosition].whatIs == Player.WhatIs.GOALKEEPER_OTHER
            ) {
                if (goalkeeperPosiblyPositions.contains(clickPosition) && !goalkeeperWasMoving) {
                    goalkeeperWasMoving = true
                    movingSelectedPlayer(selectPlayerPosition, clickPosition, true)
                }
            } else
                movingSelectedPlayer(selectPlayerPosition, clickPosition, false)
        } else {
            if (manager.checkIsGoal(this, selectPlayerPosition, clickPosition))
                gameView?.movingBall(selectPlayerPosition, clickPosition)
        }
    }

    private fun movingSelectedPlayer(
        clickPosition: Int,
        selectPlayerPosition: Int,
        goalkeeper: Boolean
    ) {
        addMoveToHistory(goalkeeper)
        if (clickPosition in if (manager.whoManage == 1)
                POSITION_OTHER_GOALS else POSITION_MY_GOALS
        ) {
            manager.listMap[clickPosition] = manager.getBall(clickPosition)
            manager.changeTypeOfPlayer()
        } else
            Collections.swap(manager.listMap, selectPlayerPosition, clickPosition)

        gameView?.refreshAdapter(selectPlayerPosition, clickPosition)
    }

    private fun tryPassToMyPlayer(clickPosition: Int) {
        val positionPlayerWithBall = manager.getPositionSelectPlayer()
        if (positionPlayerWithBall != -1)
            if (manager.whoManage == 1) {
                if (manager.listMap[clickPosition]
                        .whatIs == Player.WhatIs.PLAYER_MY &&
                    manager.listMap[positionPlayerWithBall]
                        .whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL
                    && manager.passIsCorrect(positionPlayerWithBall, clickPosition)
                ) {
                    addMoveToHistory(false)
                    Collections.swap(
                        manager.listMap,
                        positionPlayerWithBall, clickPosition
                    )
                    gameView?.movingBall(positionPlayerWithBall, clickPosition)
                }
            } else {
                if (manager.listMap[clickPosition].whatIs == Player.WhatIs.PLAYER_OTHER &&
                    manager
                        .listMap[positionPlayerWithBall].whatIs == Player
                        .WhatIs.PLAYER_OTHER_WITH_BALL
                    && manager.passIsCorrect(positionPlayerWithBall, clickPosition)
                ) {
                    addMoveToHistory(false)
                    Collections.swap(manager.listMap, positionPlayerWithBall, clickPosition)
                    gameView?.movingBall(positionPlayerWithBall, clickPosition)
                }
            }
    }

    private fun addMoveToHistory(goalkeeper: Boolean) {
        val listForHistory = mutableListOf<Player>()
        manager.listMap.forEach { listForHistory.add(it) }

        manager.listHistory.add(listForHistory)
        if (!goalkeeper)
            gameView?.hintOneBot(++incrementalShowsBot)
        else
            gameView?.dismissGoalkeeperBot()
    }

    private fun movingWasGoalkeeper(
        listOld: MutableList<Player>,
        listNew: MutableList<Player>
    ): Boolean {
        var increment = 0
        var goalkeeperPositionOld = 0
        listOld.forEach {
            if (it.whatIs == if (manager.whoManage == 1)
                    Player.WhatIs.GOALKEEPER_MY else Player.WhatIs.GOALKEEPER_OTHER
            )
                goalkeeperPositionOld = increment
            else
                increment++
        }
        increment = 0

        var goalkeeperPositionNew = 0
        listNew.forEach {
            if (it.whatIs == if (manager.whoManage == 1)
                    Player.WhatIs.GOALKEEPER_MY else Player.WhatIs.GOALKEEPER_OTHER
            )
                goalkeeperPositionNew = increment
            else
                increment++
        }
        return goalkeeperPositionOld != goalkeeperPositionNew
    }

    private fun clickPositionForMovingIsCorrect(
        clickPosition: Int,
        selectedPlayerPosition: Int
    ): Boolean {
        return clickPosition + 1 == selectedPlayerPosition
                || clickPosition - 1 == selectedPlayerPosition
                || clickPosition + WIDTH_FIELD == selectedPlayerPosition
                || clickPosition - WIDTH_FIELD == selectedPlayerPosition
                || clickPosition + WIDTH_FIELD + 1 == selectedPlayerPosition
                || clickPosition + WIDTH_FIELD - 1 == selectedPlayerPosition
                || clickPosition - WIDTH_FIELD + 1 == selectedPlayerPosition
                || clickPosition - WIDTH_FIELD - 1 == selectedPlayerPosition
    }

    override fun onClickReturn() {
        if (manager.listHistory.isNotEmpty()) {
            val goalkeeperWasChangedPosition =
                movingWasGoalkeeper(manager.listMap, manager.listHistory.last())
            manager.listMap.clear()
            manager.listHistory.last().forEach {
                manager.listMap.add(it)
            }
            if (!goalkeeperWasChangedPosition)
                gameView?.showOneBot(incrementalShowsBot--)
            else {
                gameView?.showGoalkeeperBot()
                goalkeeperWasMoving = false
            }
            manager.listHistory.removeAt(manager.listHistory.size - 1)
        }
        gameView?.notifyDataInAdapter()
    }

    companion object {
        const val WIDTH_FIELD = 9
        const val HEIGHT_FIELD = 12
        const val NUM_OF_MY_PLAYERS_ON_START = 4
        const val NUM_OF_OTHER_PLAYERS_ON_START = 4
        const val POSITION_OF_MY_GOALKEEPER = 103
        const val POSITION_OF_OTHER_GOALKEEPER = 4
        val POSITION_OTHER_GOALS = 3..5
        val POSITION_MY_GOALS = 102..104
        val defaultPositionPlayers: MutableList<Int> = mutableListOf(4, 19, 22, 25, 82, 85, 88, 103)
        const val GOAL_DELAY = 3000L
        val goalkeeperPosiblyPositions: MutableList<Int> = mutableListOf(
            2, 3, 4, 5, 6,
            11, 12, 13, 14, 15,

            92, 93, 94, 95, 96,
            101, 102, 103, 104, 105
        )
    }
}