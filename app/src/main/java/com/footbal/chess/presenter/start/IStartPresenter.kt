package com.footbal.chess.presenter.start

import com.footbal.chess.view.start.IStartActivity

interface IStartPresenter {
    fun attachView(gameActivity: IStartActivity)
    fun detachActivity()
    fun onClickSingleButton()
    fun onClickMultiplayerButton()
    fun onClickOptionsButton()
}