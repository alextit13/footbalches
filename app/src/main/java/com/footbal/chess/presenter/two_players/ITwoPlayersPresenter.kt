package com.footbal.chess.presenter.two_players

import com.footbal.chess.view.two_players.ITwoPlayerView

interface ITwoPlayersPresenter {
    fun attachView(gameActivity: ITwoPlayerView)
    fun detachView()
    fun onHasFocuse(width: Int, height: Int)
    fun onItemClick(position: Int)
    fun onClickOnTimer()
    fun onClickReturn()
    fun onClickPickBall()
    fun onViewPrepared(isPrepared: Boolean)
}