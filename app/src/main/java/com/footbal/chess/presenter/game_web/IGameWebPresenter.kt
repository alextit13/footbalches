package com.footbal.chess.presenter.game_web

import com.footbal.chess.view.game_web.IGameWebActivity

interface IGameWebPresenter {
    fun attachView(gameActivity: IGameWebActivity)
    fun detachView()
    fun onHasFocuse(hasFocus: Boolean, width: Int, height: Int)
    fun onItemClick(position: Int)
    fun onClickOnTimer()
}