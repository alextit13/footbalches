package com.footbal.chess.presenter.chose

import com.footbal.chess.model.chose.ChoseFragmentHolder.Companion.typeFragmentChoose
import com.footbal.chess.model.chose.IWebPlayerGetManagerCallback
import com.footbal.chess.model.chose.getPlayersFromWeb
import com.footbal.chess.model.util.Gamer
import com.footbal.chess.presenter.multiplayer.MultiplayerPresenter
import com.footbal.chess.view.multiplayer.fragment.IChoseView

class ChosePresenter: IChosePresenter, IWebPlayerGetManagerCallback {

    private var view: IChoseView? = null

    override fun attachView(view: IChoseView) {
        this.view = view
        view.initViews()
        view.initListeners()
        view.showProgress()
        checkTypeOfGame()
    }

    private fun checkTypeOfGame() {
        when (typeFragmentChoose) {
            MultiplayerPresenter.TypeOfChoseFragment.WEB -> getDataFromWeb()
            MultiplayerPresenter.TypeOfChoseFragment.BLUETOOTH -> getPlayersFromBluetooth()
        }
    }

    private fun getPlayersFromBluetooth() {
        //TODO this
    }

    private fun getDataFromWeb() {
        getPlayersFromWeb(this)
    }

    override fun onWebResultOk(list: MutableList<Gamer>) {
        view?.initAdapterWithGamers(list)
        view?.dismissProgress()
    }

    override fun onWebresultError() {
        view?.dismissProgress()
    }

    override fun onClickOnGamer(gamer: Gamer) {
        startGameWithGamer(gamer)
    }

    private fun startGameWithGamer(gamer: Gamer) {
        view?.openActivityWebGame(gamer)
    }

    override fun onClickRefresh() {
        view?.showProgress()
    }

    override fun detachView() {
        view = null
    }
}