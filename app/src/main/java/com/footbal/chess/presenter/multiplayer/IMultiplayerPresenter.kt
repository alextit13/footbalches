package com.footbal.chess.presenter.multiplayer

import com.footbal.chess.view.multiplayer.IMultiplayerView

interface IMultiplayerPresenter {
    fun attachView(view: IMultiplayerView)
    fun detachActivity()
    fun onClickWebButton()
    fun onClickBluetoothButton()
    fun onClickBackImageView()
    fun onBackPressed()
    fun onClickTwoPlayerButton()
}