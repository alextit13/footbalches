package com.footbal.chess.presenter.game_system

import com.footbal.chess.model.data.ITimerCallback
import com.footbal.chess.model.data.system.DataGameSystemManager
import com.footbal.chess.model.data.system.IDataGameSystemMovingCallback
import com.footbal.chess.model.data.system.SystemPlayerManager.isMoveUser
import com.footbal.chess.model.data.system.SystemPlayerManager.movingFromSystem
import com.footbal.chess.model.util.Player
import com.footbal.chess.view.game_system.IGameSystemActivity
import java.util.*

class GameSystemPresenter : IGameSystemPresenter, IDataGameSystemMovingCallback, ITimerCallback {

    private val dataGameManager = DataGameSystemManager.getActualInstance()

    override fun timeFirstUserChange(color: Int, time: String) {

    }

    override fun timeSecondUserChange(color: Int, time: String) {

    }

    private var gameView: IGameSystemActivity? = null
    private var isPresenterCreated: Boolean = false

    override fun onPrepare(isPrepared: Boolean){
        isPresenterCreated = isPrepared
    }

    override fun attachView(gameActivity: IGameSystemActivity) {
        this.gameView = gameActivity
        this.gameView?.initViews()
        this.gameView?.initListeners()
    }

    override fun onHasFocuse(width: Int, height: Int) {
        if (isPresenterCreated) {
            gameView?.initRecyclerView(
                dataGameManager.getDataForRecycler(),
                calculateItemWidth(width),
                calculateItemHeight(height)
            )
            dataGameManager.addPlayersToMap()
            startTimer()
        }
    }

    private fun startTimer() {

    }

    override fun onClickOnTimer() {
        dataGameManager.listLastEventHistory.clear()
        makeActiveAllBots()
        isMoveUser = !isMoveUser
        if (!isMoveUser)
            movingFromSystem(this)
    }

    private fun makeActiveAllBots() {
        for (i in 1..4) {
            gameView?.showOneBot(i)
        }
    }

    override fun systemPlayingFinish() {
        gameView?.showToast("Ваш ход!")
    }

    override fun onItemClick(position: Int) {
        if (dataGameManager.listLastEventHistory.size >= 4)
            gameView?.showToast("Ходы закончились")
        else {
            tryPassToMyPlayer(position)
            val currentPlayer = dataGameManager.listMap[position]
            if (currentPlayer.whatIs == Player.WhatIs.PLAYER_MY
                || currentPlayer.whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL
            )
                changeSelectionPlayer(position)
            if (currentPlayer.whatIs == Player.WhatIs.CELL)
                if (isMoveUser)
                    movingPlayerFromUser(position)
                else
                    gameView?.showToast("Ход соперника")
        }
    }

    override fun onClickReturn() {
        if (dataGameManager.listLastEventHistory.isNotEmpty()) {
            val pair = dataGameManager.listLastEventHistory.last()
            movingPlayerFromHistory(pair)
            gameView?.showOneBot(dataGameManager.listLastEventHistory.size)
            dataGameManager.listLastEventHistory
                .removeAt(dataGameManager.listLastEventHistory.size - 1)
        }
    }

    private fun changeSelectionPlayer(position: Int) {
        dataGameManager.listMap.forEach { it.isSelection = false }
        dataGameManager.listMap[position].isSelection = true
        gameView?.maximazePlayer(position)
    }

    private fun movingPlayerFromUser(clickPosition: Int) {
        val selectPlayerPosition = dataGameManager.getPositionSelectPlayer()
        if (clickPositionForMovingIsCorrect(clickPosition, selectPlayerPosition)) {
            if (clickPosition in POSITION_OTHER_GOALS) {
                dataGameManager.listMap[clickPosition] =
                    dataGameManager.getBall(clickPosition)
                dataGameManager.changeTypeOfPlayer()
            } else
                Collections.swap(dataGameManager.listMap, selectPlayerPosition, clickPosition)
            addMoveToHistory(selectPlayerPosition, clickPosition)
            gameView?.refreshAdapter(selectPlayerPosition, clickPosition)
            dataGameManager.tryPickTheBall(this)
        } else {
            if (dataGameManager.checkIsGoal(this, selectPlayerPosition, clickPosition))
                gameView?.movingBall(selectPlayerPosition, clickPosition)
        }
    }

    private fun tryPassToMyPlayer(clickPosition: Int) {
        val positionPlayerWithBall = dataGameManager.getPositionSelectPlayer()
        if (dataGameManager.listMap[clickPosition].whatIs == Player.WhatIs.PLAYER_MY &&
            dataGameManager.listMap[positionPlayerWithBall].whatIs
            == Player.WhatIs.PLAYER_MY_WITH_BALL
            && dataGameManager.passIsCorrect(positionPlayerWithBall, clickPosition)
        ) {
            // если мы нажали на игрока из своей команды и
            // выделенный игрок из моей команды и он с мячом
            // то отдаем пас

            addMoveToHistory(positionPlayerWithBall, clickPosition)
            // изменяем данные игроков в листе
            Collections.swap(dataGameManager.listMap, positionPlayerWithBall, clickPosition)
            // перемещаем мяч по полю от одного игрока к другому
            gameView?.movingBall(positionPlayerWithBall, clickPosition)
        }
    }

    private fun movingPlayerFromHistory(pair: Pair<Int, Int>) {
        val selectPlayerPosition = pair.first
        val clickPosition = pair.second
        if (clickPositionForMovingIsCorrect(clickPosition, selectPlayerPosition)) {
            if (clickPosition in POSITION_OTHER_GOALS) {
                dataGameManager.listMap[clickPosition] = dataGameManager.getBall(clickPosition)
                dataGameManager.changeTypeOfPlayer()
            } else
                Collections.swap(dataGameManager.listMap, selectPlayerPosition, clickPosition)
            gameView?.refreshAdapter(selectPlayerPosition, clickPosition)
            dataGameManager.tryPickTheBall(this)
            dataGameManager.checkIsGoal(this, selectPlayerPosition, clickPosition)
        }
    }

    private fun addMoveToHistory(selectPlayerPosition: Int, clickPosition: Int) {
        val pair = Pair(selectPlayerPosition, clickPosition)
        dataGameManager.listLastEventHistory.add(pair)
        gameView?.hintOneBot(dataGameManager.listLastEventHistory.size)
    }

    private fun clickPositionForMovingIsCorrect(
        clickPosition: Int,
        selectedPlayerPosition: Int
    ): Boolean {
        return clickPosition + 1 == selectedPlayerPosition
                || clickPosition - 1 == selectedPlayerPosition
                || clickPosition + WIDTH_FIELD == selectedPlayerPosition
                || clickPosition - WIDTH_FIELD == selectedPlayerPosition
                || clickPosition + WIDTH_FIELD + 1 == selectedPlayerPosition
                || clickPosition + WIDTH_FIELD - 1 == selectedPlayerPosition
                || clickPosition - WIDTH_FIELD + 1 == selectedPlayerPosition
                || clickPosition - WIDTH_FIELD - 1 == selectedPlayerPosition
    }

    override fun movingPlayerFromSystem(from: Int, to: Int) {
        Collections.swap(dataGameManager.listMap, from, to)
        gameView?.refreshAdapter(from, to)
        dataGameManager.tryPickTheBall(this)
        dataGameManager.checkIsGoal(this, from, to)
    }

    override fun refreshData() {
        gameView?.notifyDataInAdapter()
    }

    override fun onGoal(forWho: Int) {
        if (forWho == 1) dataGameManager.countGoalMe += 1
        else dataGameManager.countGoalOther += 1
        gameView?.showToast(if (forWho == 1) "ГОЛ СОПЕРНИКУ" else "ГОЛ МНЕ")
        Thread(Runnable {
            Thread.sleep(GOAL_DELAY)
            refreshMapAndData()
        }).start()
        dataGameManager.listLastEventHistory.clear()
        makeActiveAllBots()
    }

    private fun refreshMapAndData() {
        dataGameManager.clearList()
        gameView?.refreshViews()
    }

    private fun calculateItemWidth(widthMap: Int) = widthMap / WIDTH_FIELD

    private fun calculateItemHeight(heightMap: Int) = heightMap / HEIGHT_FIELD

    override fun detachView() {
        gameView = null
    }

    override fun onTimeFinish() {
        gameView?.showFinishGameResult(
            "${dataGameManager.countGoalMe} " +
                    ": ${dataGameManager.countGoalOther}"
        )
    }

    companion object {
        const val WIDTH_FIELD = 9
        const val HEIGHT_FIELD = 12
        const val NUM_OF_MY_PLAYERS_ON_START = 4
        const val NUM_OF_OTHER_PLAYERS_ON_START = 4
        const val POSITION_OF_MY_GOALKEEPER = 103
        const val POSITION_OF_OTHER_GOALKEEPER = 4
        val POSITION_OTHER_GOALS = 3..5
        val POSITION_MY_GOALS = 103..105
        val defaultPositionPlayers: MutableList<Int> = mutableListOf(4, 20, 24, 40, 67, 83, 87, 103)
        const val GOAL_DELAY = 3000L
    }
}