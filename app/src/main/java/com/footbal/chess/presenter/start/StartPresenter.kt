package com.footbal.chess.presenter.start

import com.footbal.chess.model.chose.pushNewGamerToFirebase
import com.footbal.chess.model.preferences.createNewGamer
import com.footbal.chess.model.preferences.getGamer
import com.footbal.chess.view.start.IStartActivity

class StartPresenter : IStartPresenter {

    private var startActivity: IStartActivity? = null

    override fun attachView(gameActivity: IStartActivity) {
        this.startActivity = gameActivity
        startActivity?.initViews()
        startActivity?.initClickers()
        generateMyGamer()
    }

    private fun generateMyGamer() {
        if (getGamer() == null) {
            createNewGamer()
            pushNewGamerToFirebase(getGamer())
        }
    }
    override fun onClickSingleButton() {
        startActivity?.onStartGameActivity()
    }

    override fun onClickMultiplayerButton() {
        startActivity?.onStartMultiplayerActivity()
    }

    override fun onClickOptionsButton() {
        //startActivity?.onStartMultiplayerActivity(GameSystemActivity::javaClass)
    }

    override fun detachActivity() {
        startActivity = null
    }
}