package com.footbal.chess.presenter.multiplayer

import com.footbal.chess.model.chose.ChoseFragmentHolder
import com.footbal.chess.view.multiplayer.IMultiplayerView
import com.footbal.chess.view.multiplayer.fragment.ChoseMultiplayerFragment

class MultiplayerPresenter : IMultiplayerPresenter {

    private var view: IMultiplayerView? = null

    override fun attachView(view: IMultiplayerView) {
        this.view = view
        view.initViews()
        view.initListeners()
    }

    override fun onClickWebButton() {
        ChoseFragmentHolder.typeFragmentChoose = TypeOfChoseFragment.WEB
        view?.openChoseFragment(ChoseMultiplayerFragment())
        view?.dismissAllFields()
    }

    override fun onClickBluetoothButton() {
        ChoseFragmentHolder.typeFragmentChoose = TypeOfChoseFragment.BLUETOOTH
        view?.openChoseFragment(ChoseMultiplayerFragment())
        view?.dismissAllFields()
    }

    override fun onClickTwoPlayerButton() {
        view?.openTwoPlayerGame()
    }

    override fun onClickBackImageView() {
        view?.closeActivity()
    }

    override fun onBackPressed() {
        view?.showAllFields()
    }

    override fun detachActivity() {
        view = null
    }

    enum class TypeOfChoseFragment {
        WEB,
        BLUETOOTH
    }
}