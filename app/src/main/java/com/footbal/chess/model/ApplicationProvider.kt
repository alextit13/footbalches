package com.footbal.chess.model

import android.app.Application
import android.content.Context

class ApplicationProvider : Application() {

    companion object {
        lateinit var currentContext: Context
    }

    fun getContextOfBase(): Context? {
        return currentContext
    }

    override fun onCreate() {
        super.onCreate()
        currentContext = applicationContext
    }
}