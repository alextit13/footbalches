package com.footbal.chess.model.chose

import com.footbal.chess.model.preferences.getGamer
import com.footbal.chess.model.util.Gamer
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

const val MAIN_GAMER_WEB_LINK = "gamers"

fun getPlayersFromWeb(callback: IWebPlayerGetManagerCallback) {
    FirebaseDatabase.getInstance().reference.child(MAIN_GAMER_WEB_LINK)
        .addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val listGamers: MutableList<Gamer> = mutableListOf()
                for (data in dataSnapshot.children) {
                    val currentGamer = data.getValue(Gamer::class.java)
                    if (currentGamer?.name != getGamer().name)
                        currentGamer?.let { listGamers.add(it) }
                }
                callback.onWebResultOk(listGamers)
            }

            override fun onCancelled(p0: DatabaseError) {
                callback.onWebresultError()
            }
        })
}

fun pushNewGamerToFirebase(gamer: Gamer) {
    FirebaseDatabase.getInstance().reference.child(MAIN_GAMER_WEB_LINK)
        .child(gamer.name)
        .setValue(gamer)
}