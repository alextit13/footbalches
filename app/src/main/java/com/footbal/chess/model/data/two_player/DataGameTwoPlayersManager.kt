package com.footbal.chess.model.data.two_player

import com.footbal.chess.model.util.Player
import com.footbal.chess.presenter.two_players.TwoPlayersPresenter

class DataGameTwoPlayersManager {

    var countGoalMe = 0
    var countGoalOther = 0
    var listMap: MutableList<Player> = mutableListOf()
    val listHistory: MutableList<MutableList<Player>> = mutableListOf()
    var whoManage = 1 // 1 - I, 2 - other player

    fun getDataForRecycler(): MutableList<Player> {
        return if (listMap.isEmpty()) {
            for (i in 0 until TwoPlayersPresenter.WIDTH_FIELD * TwoPlayersPresenter.HEIGHT_FIELD) {
                listMap.add(Player().apply {
                    whatIs = Player.WhatIs.CELL
                })
            }
            listMap
        } else listMap
    }

    fun addPlayersToMap() {
        var positionOnMap = 0
        for (i in 0 until TwoPlayersPresenter.NUM_OF_OTHER_PLAYERS_ON_START) {
            val player = generateNewPlayer(
                false,
                positionOnMap
            )
            listMap[player.positionOnMap] = player
            positionOnMap++
        }

        for (i in 0 until TwoPlayersPresenter.NUM_OF_MY_PLAYERS_ON_START) {
            val player = generateNewPlayer(
                true,
                positionOnMap
            )
            if (i == 1) {
                player.whatIs =
                    if (whoManage == 1) Player.WhatIs.PLAYER_MY_WITH_BALL else Player.WhatIs.PLAYER_OTHER_WITH_BALL
            }
            listMap[player.positionOnMap] = player
            positionOnMap++
        }

        if (whoManage == 1) {
            listMap[TwoPlayersPresenter.POSITION_OF_OTHER_GOALKEEPER] = Player()
                .apply { whatIs = Player.WhatIs.GOALKEEPER_OTHER }
            listMap[TwoPlayersPresenter.POSITION_OF_MY_GOALKEEPER] = Player()
                .apply { whatIs = Player.WhatIs.GOALKEEPER_MY }
        } else {
            listMap[TwoPlayersPresenter.POSITION_OF_MY_GOALKEEPER] = Player()
                .apply { whatIs = Player.WhatIs.GOALKEEPER_MY }
            listMap[TwoPlayersPresenter.POSITION_OF_OTHER_GOALKEEPER] = Player()
                .apply { whatIs = Player.WhatIs.GOALKEEPER_OTHER }
        }
    }

    fun removeAllSelectPlayers() {
        listMap.forEach {
            it.isSelection = false
        }
    }

    fun getPositionSelectPlayer(): Int {
        var positionOfSelectPlayer = 0
        listMap.forEach {
            if (it.isSelection)
                return positionOfSelectPlayer
            else positionOfSelectPlayer++
        }
        return -1
    }

    private fun generateNewPlayer(isMyCommand: Boolean, positionOnMap: Int) = Player().apply {
        this.positionOnMap = TwoPlayersPresenter.defaultPositionPlayers[positionOnMap]
        if (whoManage == 1) {
            whatIs = if (isMyCommand) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER
        } else {
            whatIs = if (isMyCommand) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY
        }
    }

    fun getBall(clickPosition: Int) = Player().apply {
        whatIs = Player.WhatIs.BALL
        positionOnMap = clickPosition
    }

    fun changeTypeOfPlayer() {
        if (whoManage == 1) {
            listMap.forEach {
                if (it.isSelection)
                    it.whatIs = Player.WhatIs.PLAYER_MY
            }
        } else {
            listMap.forEach {
                if (it.isSelection)
                    it.whatIs = Player.WhatIs.PLAYER_OTHER
            }
        }
    }

    fun tryPickTheBall(callback: IDataGameTwoPlayerMovingCallback) {
        var isTryWasSuccess = false
        fun getPositionOnPlayerWithBall(): Int {
            var position = 0
            var positionPlayerWithBall = 0
            listMap.forEach {
                if (whoManage == 1) {
                    if (it.whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL || it.whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL)
                        positionPlayerWithBall = position
                    else position++
                } else {
                    if (it.whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL || it.whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL)
                        positionPlayerWithBall = position
                    else position++
                }
            }
            return positionPlayerWithBall
        }

        val positionPlayerWithBall = getPositionOnPlayerWithBall()
        val positionCurrentSelectionPlayer =
            getPlayerNear(positionPlayerWithBall)

        if (whoManage == 1) {
            if (listMap[positionPlayerWithBall].whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL) {
                listMap[positionCurrentSelectionPlayer].whatIs = Player.WhatIs.PLAYER_MY_WITH_BALL
                listMap[positionPlayerWithBall].whatIs = Player.WhatIs.PLAYER_OTHER
                isTryWasSuccess = true
            }
        } else if (whoManage == 2) {
            if (listMap[positionPlayerWithBall].whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL) {
                listMap[positionCurrentSelectionPlayer].whatIs = Player.WhatIs.PLAYER_OTHER_WITH_BALL
                listMap[positionPlayerWithBall].whatIs = Player.WhatIs.PLAYER_MY
                isTryWasSuccess = true
            }
        }
        callback.refreshData(isTryWasSuccess)
    }

    private fun getPlayerNear(positionPlayerWithBall: Int): Int {
        var nearPositionPlayer = 0
        val top = positionPlayerWithBall - TwoPlayersPresenter.WIDTH_FIELD
        val right = positionPlayerWithBall + 1
        val bottom = positionPlayerWithBall + TwoPlayersPresenter.WIDTH_FIELD
        val left = positionPlayerWithBall - 1

        val leftTop = top - 1
        val rightTop = top + 1
        val rightBottom = bottom + 1
        val leftBottom = bottom - 1

        if (listMap[positionPlayerWithBall].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY_WITH_BALL else Player.WhatIs.PLAYER_OTHER_WITH_BALL) {
            // мой игрок с мячом, ищем рядом чужого игрока

            if (top > 0 && right < listMap.size && bottom < listMap.size && left > 0) {
                // мой игрок с мячом в поле, индексы рядом - тоже в поле
                when {
                    listMap[top].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY -> nearPositionPlayer =
                        top
                    listMap[right].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY -> nearPositionPlayer =
                        right
                    listMap[bottom].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY -> nearPositionPlayer =
                        bottom
                    listMap[left].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY -> nearPositionPlayer =
                        left

                    listMap[leftTop].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY -> nearPositionPlayer =
                        leftTop
                    listMap[rightTop].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY -> nearPositionPlayer =
                        rightTop
                    listMap[rightBottom].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY -> nearPositionPlayer =
                        rightBottom
                    listMap[leftBottom].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER else Player.WhatIs.PLAYER_MY -> nearPositionPlayer =
                        leftBottom
                }
            }
        } else if (listMap[positionPlayerWithBall].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_OTHER_WITH_BALL else Player.WhatIs.PLAYER_MY_WITH_BALL) {
            // другой игрок с мячом, ищем рядом моего игрока

            if (top > 0 && right < listMap.size && bottom < listMap.size && left > 0) {
                // другой игрок с мячом в поле, индексы рядом - тоже в поле
                when {
                    listMap[top].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer =
                        top
                    listMap[right].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer =
                        right
                    listMap[bottom].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer =
                        bottom
                    listMap[left].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer =
                        left

                    listMap[leftTop].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer =
                        leftTop
                    listMap[rightTop].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer =
                        rightTop
                    listMap[rightBottom].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer =
                        rightBottom
                    listMap[leftBottom].whatIs == if (whoManage == 1) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer =
                        leftBottom
                }
            }
        }

        return nearPositionPlayer
    }

    fun checkIsGoal(callback: IDataGameTwoPlayerMovingCallback, from: Int, to: Int): Boolean {
        try {
            if (whoManage == 1) {
                if (from < TwoPlayersPresenter.WIDTH_FIELD * TwoPlayersPresenter.HEIGHT_FIELD / 2) {
                    if (listMap[from].whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL && TwoPlayersPresenter.POSITION_OTHER_GOALS.contains(
                            to
                        )
                    ) {
                        if (passIsCorrect(from, to)) {
                            callback.onGoal(1)
                            return true
                        }
                    }
                }
            } else {
                if (from > TwoPlayersPresenter.WIDTH_FIELD * TwoPlayersPresenter.HEIGHT_FIELD / 2) {
                    if (listMap[from].whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL && TwoPlayersPresenter.POSITION_MY_GOALS.contains(
                            to
                        )
                    ) {
                        if (passIsCorrect(from, to)) {
                            callback.onGoal(1)
                            return true
                        }
                    }
                }
            }
            return false
        } catch (e: Exception) {
            return false
        }
    }

    fun clearList() {
        listMap.clear()
    }

    fun passIsCorrect(positionPlayerWithBall: Int, clickPosition: Int): Boolean {
        val listPsevdoCoordinatesPlayresOnScreen = getPsevdocoordinates()

        val pairFrom = listPsevdoCoordinatesPlayresOnScreen[positionPlayerWithBall]
        val pairTo = listPsevdoCoordinatesPlayresOnScreen[clickPosition]

        if (Math.abs(pairFrom.first) == Math.abs(pairTo.first) ||
            Math.abs(pairFrom.second) == Math.abs(pairTo.second)
        )
            return true

        var isPairFirstMax = false

        val maxX: Int
        val minX: Int
        if (pairFrom.first >= pairTo.first) {
            isPairFirstMax = true
            maxX = pairFrom.first
            minX = pairTo.first
        } else {
            maxX = pairTo.first
            minX = pairFrom.first
        }
        val firstResult = maxX - minX
        var twoResult = 0
        twoResult = if (isPairFirstMax) {
            pairFrom.second - pairTo.second
        } else {
            pairTo.second - pairFrom.second
        }

        return Math.abs(firstResult) == Math.abs(twoResult)
    }

    private fun getPsevdocoordinates(): MutableList<Pair<Int, Int>> {
        val list: MutableList<Pair<Int, Int>> = mutableListOf()
        var rowIsCancel = 0
        var x = 0
        var y = 0
        repeat(listMap.size) {
            val pair = Pair(x, y)
            list.add(pair)
            if (rowIsCancel == TwoPlayersPresenter.WIDTH_FIELD - 1) {
                x = 0
                y++
                rowIsCancel = 0
            } else {
                x++
                rowIsCancel++
            }
        }

        return list
    }

    companion object {
        private var instance: DataGameTwoPlayersManager? = null
        fun getActualIntance(): DataGameTwoPlayersManager {
            if (instance == null) {
                instance = DataGameTwoPlayersManager()
            }
            return instance as DataGameTwoPlayersManager
        }
    }
}