package com.footbal.chess.model.chose

import com.footbal.chess.model.util.Gamer

interface IWebPlayerGetManagerCallback {
    fun onWebResultOk(list: MutableList<Gamer>)
    fun onWebresultError()
}