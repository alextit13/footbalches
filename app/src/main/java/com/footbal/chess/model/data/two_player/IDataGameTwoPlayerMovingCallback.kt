package com.footbal.chess.model.data.two_player

interface IDataGameTwoPlayerMovingCallback {
    fun movingPlayerFromOtherPlayer(from: Int, to: Int)
    fun onGoal(forWho: Int)
    fun refreshData(isTryWasSuccess: Boolean)
}