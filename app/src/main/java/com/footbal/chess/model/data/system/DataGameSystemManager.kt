package com.footbal.chess.model.data.system

import com.footbal.chess.model.util.Player
import com.footbal.chess.presenter.game_system.GameSystemPresenter
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.HEIGHT_FIELD
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.POSITION_OF_MY_GOALKEEPER
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.POSITION_OF_OTHER_GOALKEEPER
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.POSITION_OTHER_GOALS
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.WIDTH_FIELD
import kotlin.math.abs

class DataGameSystemManager {

    var countGoalMe = 0
    var countGoalOther = 0
    var listMap: MutableList<Player> = mutableListOf()
    val listLastEventHistory: MutableList<Pair<Int, Int>> = mutableListOf()

    fun getDataForRecycler(): MutableList<Player> {
        return if (listMap.isEmpty()) {
            for (i in 0 until WIDTH_FIELD * HEIGHT_FIELD) {
                listMap.add(Player().apply {
                    whatIs = Player.WhatIs.CELL
                })
            }
            listMap
        } else listMap
    }

    fun addPlayersToMap() {
        var positionOnMap = 0
        for (i in 0 until GameSystemPresenter.NUM_OF_OTHER_PLAYERS_ON_START) {
            val player = generateNewPlayer(
                false,
                positionOnMap
            )
            listMap[player.positionOnMap] = player
            positionOnMap++
        }

        for (i in 0 until GameSystemPresenter.NUM_OF_MY_PLAYERS_ON_START) {
            val player = generateNewPlayer(
                true,
                positionOnMap
            )
            if (i == 0)
                player.whatIs = Player.WhatIs.PLAYER_MY_WITH_BALL
            listMap[player.positionOnMap] = player
            positionOnMap++
        }

        listMap[POSITION_OF_OTHER_GOALKEEPER] = Player()
            .apply { whatIs = Player.WhatIs.GOALKEEPER_OTHER }
        listMap[POSITION_OF_MY_GOALKEEPER] = Player()
            .apply { whatIs = Player.WhatIs.GOALKEEPER_MY }
    }

    fun getPositionSelectPlayer(): Int {
        var positionOfSelectPlayer = 0
        listMap.forEach {
            if (it.whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL)
                return positionOfSelectPlayer
            else positionOfSelectPlayer++
        }
        return -1
    }

    private fun generateNewPlayer(isMyCommand: Boolean, positionOnMap: Int) = Player().apply {
        this.positionOnMap = GameSystemPresenter.defaultPositionPlayers[positionOnMap]
        whatIs = if (isMyCommand) Player.WhatIs.PLAYER_MY else Player.WhatIs.PLAYER_OTHER
    }

    fun getBall(clickPosition: Int) = Player().apply {
        whatIs = Player.WhatIs.BALL
        positionOnMap = clickPosition
    }

    fun changeTypeOfPlayer() {
        listMap.forEach {
            if (it.isSelection)
                it.whatIs = Player.WhatIs.PLAYER_MY
        }
    }

    fun tryPickTheBall(callback: IDataGameSystemMovingCallback) {
        fun getPositionOnPlayerWithBall(): Int {
            var position = 0
            var positionPlayerWithBall = 0
            listMap.forEach {
                if (it.whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL || it.whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL)
                    positionPlayerWithBall = position
                else position++
            }
            return positionPlayerWithBall
        }

        val positionPlayerWithBall = getPositionOnPlayerWithBall()
        val nearPlayerPosition =
            getPlayerNear(positionPlayerWithBall)

        if (listMap[nearPlayerPosition].whatIs == Player.WhatIs.PLAYER_OTHER) {
            listMap[nearPlayerPosition].whatIs = Player.WhatIs.PLAYER_OTHER_WITH_BALL
            listMap[positionPlayerWithBall].whatIs = Player.WhatIs.PLAYER_MY
        } else if (listMap[nearPlayerPosition].whatIs == Player.WhatIs.PLAYER_MY) {
            listMap[nearPlayerPosition].whatIs = Player.WhatIs.PLAYER_MY_WITH_BALL
            listMap[positionPlayerWithBall].whatIs = Player.WhatIs.PLAYER_OTHER
        }
        callback.refreshData()
    }

    private fun getPlayerNear(positionPlayerWithBall: Int): Int {
        var nearPositionPlayer = 0
        val top = positionPlayerWithBall - WIDTH_FIELD
        val right = positionPlayerWithBall + 1
        val bottom = positionPlayerWithBall + WIDTH_FIELD
        val left = positionPlayerWithBall - 1
        if (listMap[positionPlayerWithBall].whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL) {
            // мой игрок с мячом, ищем рядом чужого игрока

            if (top > 0 && right < listMap.size && bottom < listMap.size && left > 0) {
                // мой игрок с мячом в поле, индексы рядом - тоже в поле
                when {
                    listMap[top].whatIs == Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer = top
                    listMap[right].whatIs == Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer = right
                    listMap[bottom].whatIs == Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer = bottom
                    listMap[left].whatIs == Player.WhatIs.PLAYER_OTHER -> nearPositionPlayer = left
                }
            }
        } else if (listMap[positionPlayerWithBall].whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL) {
            // другой игрок с мячом, ищем рядом моего игрока

            if (top > 0 && right < listMap.size && bottom < listMap.size && left > 0) {
                // другой игрок с мячом в поле, индексы рядом - тоже в поле
                when {
                    listMap[top].whatIs == Player.WhatIs.PLAYER_MY -> nearPositionPlayer = top
                    listMap[right].whatIs == Player.WhatIs.PLAYER_MY -> nearPositionPlayer = right
                    listMap[bottom].whatIs == Player.WhatIs.PLAYER_MY -> nearPositionPlayer = bottom
                    listMap[left].whatIs == Player.WhatIs.PLAYER_MY -> nearPositionPlayer = left
                }
            }
        }

        return nearPositionPlayer
    }

    fun checkIsGoal(callback: IDataGameSystemMovingCallback, from: Int, to: Int): Boolean {
        if (from < WIDTH_FIELD * HEIGHT_FIELD / 2) {
            if (listMap[from].whatIs == Player.WhatIs.PLAYER_MY_WITH_BALL &&
                POSITION_OTHER_GOALS.contains(to)) {
                if (passIsCorrect(from, to)) {
                    callback.onGoal(1)
                    return true
                }
            }
        }
        return false
    }

    fun clearList() {
        listMap.removeAll(listMap)
    }

    fun passIsCorrect(positionPlayerWithBall: Int, clickPosition: Int): Boolean {
        val listPsevdoCoordinatesPlayresOnScreen = getPsevdocoordinates()

        val pairFrom = listPsevdoCoordinatesPlayresOnScreen[positionPlayerWithBall]
        val pairTo = listPsevdoCoordinatesPlayresOnScreen[clickPosition]

        if (abs(pairFrom.first) == abs(pairTo.first) ||
            abs(pairFrom.second) == abs(pairTo.second)
        )
            return true

        var isPairFirstMax = false

        val maxX: Int
        val minX: Int
        if (pairFrom.first >= pairTo.first) {
            isPairFirstMax = true
            maxX = pairFrom.first
            minX = pairTo.first
        } else {
            maxX = pairTo.first
            minX = pairFrom.first
        }
        val firstResult = maxX - minX
        val twoResult: Int
        twoResult = if (isPairFirstMax) {
            pairFrom.second - pairTo.second
        } else {
            pairTo.second - pairFrom.second
        }

        return abs(firstResult) == abs(twoResult)
    }

    private fun getPsevdocoordinates(): MutableList<Pair<Int, Int>> {
        val list: MutableList<Pair<Int, Int>> = mutableListOf()
        var rowIsCancel = 0
        var x = 0
        var y = 0
        repeat(listMap.size) {
            val pair = Pair(x, y)
            list.add(pair)
            if (rowIsCancel == WIDTH_FIELD - 1) {
                x = 0
                y++
                rowIsCancel = 0
            } else {
                x++
                rowIsCancel++
            }
        }

        return list
    }

    companion object {

        private var instance: DataGameSystemManager? = null

        fun getActualInstance(): DataGameSystemManager {
            if (instance == null) {
                instance = DataGameSystemManager()
            }
            return instance as DataGameSystemManager
        }
    }
}