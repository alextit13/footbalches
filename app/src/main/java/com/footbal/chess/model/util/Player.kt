package com.footbal.chess.model.util

class Player {

    var whatIs = WhatIs.CELL
    var isSelection: Boolean = false
    var positionOnMap: Int = 0

    enum class WhatIs {
        PLAYER_MY,
        PLAYER_OTHER,
        PLAYER_MY_WITH_BALL,
        PLAYER_OTHER_WITH_BALL,
        BALL,
        CELL,
        GOALKEEPER_MY,
        GOALKEEPER_OTHER,
        PLAYER_MY_SELECT,
        PLAYER_OTHER_SELECT
    }
}