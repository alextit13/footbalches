package com.footbal.chess.model.data.system

interface IDataGameSystemMovingCallback {
    fun movingPlayerFromSystem(from: Int, to: Int)
    fun systemPlayingFinish()
    fun onGoal(forWho: Int)
    fun refreshData()
}