package com.footbal.chess.model.preferences

import android.content.Context
import com.footbal.chess.model.ApplicationProvider
import com.footbal.chess.model.util.Gamer
import com.google.gson.Gson
import java.util.*

const val SP_NAME = "sp_database"
const val SP_GAMER = "sp_gamer"

fun createNewGamer() {
    val gamer = Gamer()
    gamer.name = "Gamer_" + Date().time
    ApplicationProvider.currentContext.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        .edit()
        .putString(SP_GAMER, Gson().toJson(gamer))
        .apply()
}

fun getGamer() = Gson().fromJson(
    ApplicationProvider.currentContext.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        .getString(SP_GAMER, ""), Gamer::class.java
)