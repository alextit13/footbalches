package com.footbal.chess.model.data.web

import com.footbal.chess.model.data.system.IDataGameWebMovingCallback
import com.footbal.chess.model.util.Player
import com.footbal.chess.model.data.web.DataGameWebManager.Companion.listMap
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.HEIGHT_FIELD
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.WIDTH_FIELD

object WebPlayerManager {

    private const val DELAY_BETWEEN_MOVES = 1000L

    private var currentCallback: IDataGameWebMovingCallback? = null
    var isMoveUser = true
    private var playerOnePosition = 0
    private var playerTwoPosition = 0
    private var playerThreePosition = 0
    private var playerGoalkeeperPosition = 0
    // 2 если нельзя то рандомно ходит тремя разными игроками, желательно в сторону ворот противника!

    fun movingFromSystem(callback: IDataGameWebMovingCallback) {
        currentCallback = callback
        // 1 определяем позиции наших игроков на поле
        calculatePositionOtherPlayersOnField()
        // есть ли у нас мяч
        if (getUserWithBall().whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL) {
            // TODO 1
        } else {
            // мяча у нас нету, ходим три случайных хода!!!
            makeThreeRandomMove()
        }
    }

    private fun calculatePositionOtherPlayersOnField() {
        var iterator = 0
        var iteratorPlayers = 0

        listMap.forEach {
            when (it.whatIs) {
                Player.WhatIs.GOALKEEPER_OTHER -> playerGoalkeeperPosition = iterator
                Player.WhatIs.PLAYER_OTHER -> when (iteratorPlayers) {
                    0 -> {
                        playerOnePosition = iterator
                        iteratorPlayers++
                    }
                    1 -> {
                        playerTwoPosition = iterator
                        iteratorPlayers++
                    }
                    2 -> {
                        playerThreePosition = iterator
                        iteratorPlayers++
                    }
                    3 -> return
                }
                else -> {
                }
            }
            iterator++
        }
    }

    private fun makeThreeRandomMove() {
        // кем будем ходить - три числа. 1 - первый, 2 - второй, 3 - третий, 4 - голкипер

        Thread(Runnable {
            for (i in 0..3) {
                when (getRandomNumber()) {
                    1 -> movePlayer(
                        playerOnePosition,
                        getRandomNumberOnMap(playerOnePosition)
                    )
                    2 -> movePlayer(
                        playerTwoPosition,
                        getRandomNumberOnMap(playerTwoPosition)
                    )
                    3 -> movePlayer(
                        playerThreePosition,
                        getRandomNumberOnMap(playerThreePosition)
                    )
                    4 -> movePlayer(
                        playerGoalkeeperPosition,
                        getRandomNumberForGoalkeeper()
                    )
                }
                Thread.sleep(DELAY_BETWEEN_MOVES)
            }
            isMoveUser = true
            currentCallback?.systemPlayingFinish()
        }).start()
    }

    private fun movePlayer(from: Int, to: Int) {
        currentCallback?.movingPlayerFromSystem(from, to)
    }

    private fun getRandomNumber() = (1..5).random()

    private fun getRandomNumberOnMap(playerPosition: Int): Int {
        val randomNumber = (0 until WIDTH_FIELD * HEIGHT_FIELD).random()
        return if (randomNumberIsCorrect(
                randomNumber,
                playerPosition
            )
        )
            randomNumber
        else
            getRandomNumberOnMap(playerPosition)
    }

    private fun randomNumberIsCorrect(randomNumber: Int, playerPosition: Int): Boolean {
        return playerPosition + 1 == randomNumber || playerPosition - 1 == randomNumber
                || playerPosition + WIDTH_FIELD == randomNumber || playerPosition - WIDTH_FIELD == randomNumber
                || playerPosition + WIDTH_FIELD + 1 == randomNumber || playerPosition + WIDTH_FIELD - 1 == randomNumber
                || playerPosition - WIDTH_FIELD + 1 == randomNumber || playerPosition - WIDTH_FIELD - 1 == randomNumber
    }

    private fun getRandomNumberForGoalkeeper() = (3..5).random()

    private fun getUserWithBall(): Player {
        listMap.forEach {
            if (it.whatIs == Player.WhatIs.PLAYER_OTHER_WITH_BALL)
                return it
        }
        return Player().apply {
            this.positionOnMap = -1
        }
    }
}