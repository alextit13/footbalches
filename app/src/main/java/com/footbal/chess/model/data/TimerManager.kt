package com.footbal.chess.model.data

import android.graphics.Color

object TimerManager {

    var timeFirstPlayer = 180_000L
    var timeSecondPlayer = 180_000L

    var isFirstTimerRunning = false
    var isSecondTimerRunning = false

    fun goTimerFirstPlayer(callback: ITimerCallback) {
        if (!isFirstTimerRunning) {
            isFirstTimerRunning = true
            Thread(Runnable {
                while (timeFirstPlayer >= 0) {
                    if (!isFirstTimerRunning)
                        break
                    callback.timeFirstUserChange(Color.BLUE, getTimeFromLong(timeFirstPlayer))
                    timeFirstPlayer -= 1000L
                    Thread.sleep(1000)
                }
                if (timeFirstPlayer <= 0)
                    callback.onTimeFinish()
            }).start()
        }
    }

    fun goTimerSecondPlayer(callback: ITimerCallback) {
        if (!isSecondTimerRunning) {
            isSecondTimerRunning = true
            Thread(Runnable {
                while (timeSecondPlayer >= 0) {
                    if (!isSecondTimerRunning)
                        break
                    callback.timeSecondUserChange(Color.RED, getTimeFromLong(timeSecondPlayer))
                    timeSecondPlayer -= 1000L
                    Thread.sleep(1000)
                }
                if (timeSecondPlayer <= 0)
                    callback.onTimeFinish()
            }).start()
        }
    }

    fun stopTimerFirstPlayer(){
        isFirstTimerRunning = false
    }

    fun stopTimerSecondPlayer(){
        isSecondTimerRunning = false
    }

    private fun getTimeFromLong(timeLong: Long): String {
        val seconds = (timeLong / 1000).toInt() % 60
        val minutes = (timeLong / (1000 * 60) % 60).toInt()
        return "$minutes : $seconds"
    }
}

interface ITimerCallback {
    fun timeFirstUserChange(color: Int, time: String)
    fun timeSecondUserChange(color: Int, time: String)
    fun onTimeFinish()
}