package com.footbal.chess.model.chose

import com.footbal.chess.presenter.multiplayer.MultiplayerPresenter

class ChoseFragmentHolder {
    companion object {
        var typeFragmentChoose: MultiplayerPresenter.TypeOfChoseFragment? = null
    }
}