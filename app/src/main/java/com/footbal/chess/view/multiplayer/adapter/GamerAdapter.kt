package com.footbal.chess.view.multiplayer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.footbal.chess.R
import com.footbal.chess.model.util.Gamer
import kotlinx.android.synthetic.main.item_gamer.view.*

class GamerAdapter(private val listGamers: MutableList<Gamer>, private val layoutInflater: LayoutInflater) : BaseAdapter() {

    override fun getView(position: Int, parent: View?, group: ViewGroup?): View {
        val rootView: View = layoutInflater.inflate(R.layout.item_gamer, group, false)
        rootView.text_view_gamer_name.text = listGamers[position].name
        return rootView
    }

    override fun getItem(position: Int) = listGamers[position]

    override fun getItemId(position: Int) = listGamers[position].hashCode().toLong()

    override fun getCount() = listGamers.size
}