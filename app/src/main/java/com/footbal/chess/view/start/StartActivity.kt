package com.footbal.chess.view.start

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.footbal.chess.R
import com.footbal.chess.presenter.start.IStartPresenter
import com.footbal.chess.presenter.start.StartPresenter
import com.footbal.chess.view.game_system.GameSystemActivity
import com.footbal.chess.view.multiplayer.MultiplayerActivity
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_main.*

class StartActivity : AppCompatActivity(), IStartActivity {

    private var presenter: IStartPresenter? = null
    private lateinit var singleButton: Button
    private lateinit var multiplayerButton: Button
    private lateinit var optionsButton: Button
    private lateinit var engButton: Button
    private lateinit var rusButton: Button
    private lateinit var grButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FirebaseApp.initializeApp(this)
    }

    override fun onResume() {
        super.onResume()
        attachPresenter()
    }

    private fun attachPresenter() {
        if (lastNonConfigurationInstance != null) {
            presenter = lastNonConfigurationInstance as IStartPresenter
        }
        if (presenter == null) {
            presenter = StartPresenter()
        }
        presenter?.attachView(this)
    }

    override fun initViews() {
        singleButton = button_single
        multiplayerButton = button_multiplayers
        optionsButton = button_options
        engButton = button_eng
        rusButton = button_rus
        grButton = button_gruz
    }

    override fun initClickers() {
        singleButton.setOnClickListener {
            presenter?.onClickSingleButton()
        }
        multiplayerButton.setOnClickListener {
            presenter?.onClickMultiplayerButton()
        }
        optionsButton.setOnClickListener {
            presenter?.onClickOptionsButton()
        }
    }

    override fun onStartGameActivity() {
        startActivity(Intent(this, GameSystemActivity::class.java))
    }

    override fun onStartMultiplayerActivity() {
        startActivity(Intent(this, MultiplayerActivity::class.java))
    }

    override fun onRetainCustomNonConfigurationInstance(): IStartPresenter? {
        return presenter
    }

    override fun onStop() {
        presenter?.detachActivity()
        super.onStop()
    }
}