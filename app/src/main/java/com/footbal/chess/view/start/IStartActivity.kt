package com.footbal.chess.view.start

interface IStartActivity {
    fun initViews()
    fun initClickers()
    fun onStartGameActivity()
    fun onStartMultiplayerActivity()
}