package com.footbal.chess.view.multiplayer.fragment

import com.footbal.chess.model.util.Gamer

interface IChoseView {
    fun initViews()
    fun initListeners()
    fun showProgress()
    fun initAdapterWithGamers(list: MutableList<Gamer>)
    fun dismissProgress()
    fun openActivityWebGame(gamer: Gamer)
}