package com.footbal.chess.view.multiplayer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import com.footbal.chess.R
import com.footbal.chess.presenter.multiplayer.IMultiplayerPresenter
import com.footbal.chess.presenter.multiplayer.MultiplayerPresenter
import com.footbal.chess.view.multiplayer.fragment.ChoseMultiplayerFragment
import com.footbal.chess.view.two_players.TwoPlayersActivity
import kotlinx.android.synthetic.main.activity_multiplayer.*

class MultiplayerActivity : AppCompatActivity(), IMultiplayerView {

    private var presenter: IMultiplayerPresenter? = null

    /*private lateinit var webButton: Button
    private lateinit var bluetoothButton: Button*/
    private lateinit var twoPlayerButton: Button
    private lateinit var backImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiplayer)
    }

    override fun onResume() {
        super.onResume()
        attachPresenter()
    }

    private fun attachPresenter() {
        if (lastNonConfigurationInstance != null)
            presenter = lastNonConfigurationInstance as IMultiplayerPresenter?
        if (presenter == null) {
            presenter = MultiplayerPresenter()
            presenter?.attachView(this)
        }
    }

    override fun initViews() {
        /*webButton = btn_web
        bluetoothButton = btn_bluetooth*/
        twoPlayerButton = btn_two_player
        backImageView = iv_back
    }

    override fun initListeners() {
        /*webButton.setOnClickListener { presenter?.onClickWebButton() }
        bluetoothButton.setOnClickListener { presenter?.onClickBluetoothButton() }*/
        backImageView.setOnClickListener { presenter?.onClickBackImageView() }
        twoPlayerButton.setOnClickListener { presenter?.onClickTwoPlayerButton() }
    }

    override fun openChoseFragment(choseFragment: ChoseMultiplayerFragment) {
        fragmentManager.beginTransaction().add(R.id.relative_layout_container_fragment_multiplayer, choseFragment)
            .addToBackStack(null)
            .commit()
    }

    override fun openTwoPlayerGame() {
        startActivity(Intent(this, TwoPlayersActivity::class.java))
    }

    override fun showAllFields() {
        /*webButton.visibility = View.VISIBLE
        bluetoothButton.visibility = View.VISIBLE*/
        backImageView.visibility = View.VISIBLE
    }

    override fun dismissAllFields() {
        /*webButton.visibility = View.GONE
        bluetoothButton.visibility = View.GONE*/
        backImageView.visibility = View.GONE
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter?.onBackPressed()
    }

    override fun closeActivity() {
        finish()
    }

    override fun onRetainCustomNonConfigurationInstance(): IMultiplayerPresenter? {
        return presenter
    }

    override fun onStop() {
        presenter?.detachActivity()
        super.onStop()
    }
}