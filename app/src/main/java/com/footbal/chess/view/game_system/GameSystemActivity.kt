package com.footbal.chess.view.game_system

import android.animation.Animator
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewTreeObserver
import android.widget.*
import com.footbal.chess.R
import com.footbal.chess.model.util.Player
import com.footbal.chess.presenter.game_system.GameSystemPresenter
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.WIDTH_FIELD
import com.footbal.chess.presenter.game_system.IGameSystemPresenter
import com.footbal.chess.view.game_system.adapter.GameWebMapAdapter
import com.footbal.chess.view.game_web.adapter.IOnClickItemCallback
import kotlinx.android.synthetic.main.activity_game.*

class GameSystemActivity : AppCompatActivity(), IGameSystemActivity, IOnClickItemCallback {

    private var presenter: IGameSystemPresenter? = null
    private var layoutGlobalListener: ViewTreeObserver.OnGlobalLayoutListener? = null

    private lateinit var mapRecycler: RecyclerView
    private lateinit var container: RelativeLayout
    private lateinit var containerForBall: FrameLayout
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var recyclerAdapter: GameWebMapAdapter
    private lateinit var timerTextView: TextView
    private lateinit var returnImageView: ImageView

    private lateinit var bootOne: ImageView
    private lateinit var bootTwo: ImageView
    private lateinit var bootThree: ImageView
    private lateinit var bootFour: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
    }

    override fun onResume() {
        super.onResume()
        attachPresenter()
    }

    private fun attachPresenter() {
        if (presenter == null) {
            presenter = GameSystemPresenter()
            presenter?.onPrepare(true)
        } else {
            presenter?.onPrepare(false)
        }
        presenter?.attachView(this)
    }

    override fun initViews() {
        mapRecycler = map_grid
        container = container_rl
        containerForBall = fl_container_for_ball
        timerTextView = text_view_timer
        returnImageView = image_view_return

        bootOne = image_view_bot_1
        bootTwo = image_view_bot_2
        bootThree = image_view_bot_3
        bootFour = image_view_bot_4
        layoutGlobalListener = ViewTreeObserver.OnGlobalLayoutListener {
            if (mapRecycler.width != 0
                && mapRecycler.height != 0) {
                presenter?.onHasFocuse(
                    mapRecycler.width,
                    mapRecycler.height)
                mapRecycler.viewTreeObserver.removeOnGlobalLayoutListener(
                    layoutGlobalListener
                )
            }
        }

        mapRecycler.viewTreeObserver.addOnGlobalLayoutListener(
            layoutGlobalListener
        )
    }

    override fun initListeners() {
        timerTextView.setOnClickListener { presenter?.onClickOnTimer() }
        returnImageView.setOnClickListener { presenter?.onClickReturn() }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        // presenter?.onHasFocuse(hasFocus, mapRecycler.width, mapRecycler.height)
    }

    override fun refreshViews() {
        runOnUiThread {
            onWindowFocusChanged(true)
        }
    }

    override fun initRecyclerView(
        dataForRecycler: MutableList<Player>,
        itemWidth: Int?,
        itemHeight: Int?) {
        layoutManager = GridLayoutManager(this, WIDTH_FIELD)
        recyclerAdapter = GameWebMapAdapter(
            dataForRecycler,
            this,
            itemWidth!!,
            itemHeight!!,
            this)
        mapRecycler.layoutManager = layoutManager
        mapRecycler.adapter = recyclerAdapter
        mapRecycler.recycledViewPool.setMaxRecycledViews(0, 0)
    }

    override fun refreshAdapter(from: Int, to: Int) {
        runOnUiThread { animationgViews(from, to) }
    }

    private fun animationgViews(from: Int, to: Int) {
        val viewFrom = mapRecycler.getChildAt(from)
        val viewTo = mapRecycler.getChildAt(to)

        viewFrom.animate().x(viewTo.x).y(viewTo.y)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    recyclerAdapter.notifyDataSetChanged()
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }
            })
            .start()
    }

    override fun onItemClick(position: Int) {
        presenter?.onItemClick(position)
    }

    override fun maximazePlayer(position: Int) {
        mapRecycler.getChildAt(position).animate().scaleX(1.2f).scaleY(1.2f).start()
    }

    override fun minimazePlayer(position: Int) {
        mapRecycler.getChildAt(position).animate().scaleX(1f).scaleY(1f).start()
    }

    override fun showToast(message: String) {
        runOnUiThread {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun setTimeInTextViewTime(time: String) {
        runOnUiThread { timerTextView.text = time }
    }

    override fun notifyDataInAdapter() {
        runOnUiThread {
            //recyclerAdapter.notifyDataSetChanged()
        }
    }

    override fun movingBall(from: Int, to: Int) {
        val ball = View(this)
        ball.visibility = View.GONE
        ball.setBackgroundResource(R.drawable.ball)
        ball.layoutParams = FrameLayout.LayoutParams(50, 50)
        containerForBall.addView(ball)
        val xFrom = mapRecycler.getChildAt(from).x
        val yFrom = mapRecycler.getChildAt(from).y
        ball.x = xFrom
        ball.y = yFrom
        ball.visibility = View.VISIBLE
        val xTo = mapRecycler.getChildAt(to).x
        val yTo = mapRecycler.getChildAt(to).y


        ball.animate().x(xTo).y(yTo)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    ball.visibility = View.GONE
                    recyclerAdapter.notifyDataSetChanged()
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }
            })
            .start()
    }

    override fun showFinishGameResult(result: String) {
        AlertDialog.Builder(this)
            .setTitle("ИГРА ЗАВЕРШЕНА")
            .setMessage("")
            .create().show()
    }

    override fun showOneBot(position: Int){
        when (position) {
            1 -> bootOne.setImageResource(R.drawable.interface_boot_active)
            2 -> bootTwo.setImageResource(R.drawable.interface_boot_active)
            3 -> bootThree.setImageResource(R.drawable.interface_boot_active)
            4 -> bootFour.setImageResource(R.drawable.interface_boot_active)
        }
    }

    override fun hintOneBot(position: Int){
        when (position) {
            1 -> bootOne.setImageResource(R.drawable.interface_boot_pasive)
            2 -> bootTwo.setImageResource(R.drawable.interface_boot_pasive)
            3 -> bootThree.setImageResource(R.drawable.interface_boot_pasive)
            4 -> bootFour.setImageResource(R.drawable.interface_boot_pasive)
        }
    }

    override fun onPause() {
        presenter?.detachView()
        super.onPause()
    }
}