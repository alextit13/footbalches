package com.footbal.chess.view.two_players

import android.animation.Animator
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewTreeObserver
import android.widget.*
import com.footbal.chess.R
import com.footbal.chess.model.util.Player
import com.footbal.chess.presenter.game_system.GameSystemPresenter
import com.footbal.chess.presenter.two_players.ITwoPlayersPresenter
import com.footbal.chess.presenter.two_players.TwoPlayersPresenter
import com.footbal.chess.view.game_web.adapter.IOnClickItemCallback
import com.footbal.chess.view.two_players.adapter.TwoPlayerAdapter
import kotlinx.android.synthetic.main.activity_two_player.*

class TwoPlayersActivity : AppCompatActivity(), ITwoPlayerView, IOnClickItemCallback {

    private var presenter: ITwoPlayersPresenter? = null
    private var onGlobalListener: ViewTreeObserver.OnGlobalLayoutListener? = null

    private lateinit var mapRecycler: RecyclerView
    private lateinit var container: RelativeLayout
    private lateinit var containerForBall: FrameLayout
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var recyclerAdapter: TwoPlayerAdapter
    private lateinit var timerTextView: TextView
    private lateinit var returnImageView: ImageView

    private lateinit var bootOne: ImageView
    private lateinit var bootTwo: ImageView
    private lateinit var bootThree: ImageView
    private lateinit var bootFour: ImageView
    private lateinit var pickBallImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_two_player)
    }

    override fun onResume() {
        super.onResume()
        attachPresenter()
    }

    private fun attachPresenter() {
        if (presenter == null) {
            presenter = TwoPlayersPresenter()
            presenter?.onViewPrepared(true)
        }
        presenter?.attachView(this)
    }

    override fun initViews() {
        mapRecycler = map_grid_tp
        container = container_rl_tp
        containerForBall = fl_container_for_ball_tp
        timerTextView = text_view_timer_tp
        returnImageView = image_view_return_tp

        bootOne = image_view_bot_1_tp
        bootTwo = image_view_bot_2_tp
        bootThree = image_view_bot_3_tp
        bootFour = image_view_bot_4_tp

        pickBallImageView = image_view_pick
        onGlobalListener = ViewTreeObserver.OnGlobalLayoutListener {
            if (mapRecycler.width != 0
                && mapRecycler.height != 0) {
                presenter?.onHasFocuse(
                    mapRecycler.width,
                    mapRecycler.height
                )
                mapRecycler.viewTreeObserver
                    .removeOnGlobalLayoutListener(onGlobalListener)
            }
        }

        mapRecycler.viewTreeObserver.addOnGlobalLayoutListener(onGlobalListener)
    }

    override fun initListeners() {
        timerTextView.setOnClickListener { presenter?.onClickOnTimer() }
        returnImageView.setOnClickListener { presenter?.onClickReturn() }
        pickBallImageView.setOnClickListener { presenter?.onClickPickBall() }
    }

    override fun recreateGame() {
        runOnUiThread {
            recreate()
        }
    }

    override fun refreshViews() {
        runOnUiThread {
            onWindowFocusChanged(true)
        }
    }

    override fun initRecyclerView(
        dataForRecycler: MutableList<Player>,
        itemWidth: Int?,
        itemHeight: Int?
    ) {
        layoutManager = GridLayoutManager(this, GameSystemPresenter.WIDTH_FIELD)
        recyclerAdapter = TwoPlayerAdapter(dataForRecycler, this, itemWidth!!, itemHeight!!, this)
        mapRecycler.layoutManager = layoutManager
        mapRecycler.adapter = recyclerAdapter
        mapRecycler.recycledViewPool.setMaxRecycledViews(0, 0)
    }

    override fun refreshAdapter(from: Int, to: Int) {
        runOnUiThread {
            animationgViews(from, to)
        }
    }

    private fun animationgViews(from: Int, to: Int) {
        val viewFrom = mapRecycler.getChildAt(from)
        val viewTo = mapRecycler.getChildAt(to)

        viewFrom.animate().x(viewTo.x).y(viewTo.y).withEndAction { recyclerAdapter.notifyDataSetChanged() }.start()
    }

    override fun onItemClick(position: Int) {
        presenter?.onItemClick(position)
    }


    override fun maximazePlayer(position: Int) {
        mapRecycler.getChildAt(position).animate().scaleX(1.2f).scaleY(1.2f).start()
    }

    override fun minimazePlayer(position: Int) {
        mapRecycler.getChildAt(position).animate().scaleX(1f).scaleY(1f).start()
    }

    override fun showToast(message: String) {
        runOnUiThread {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun setTimeInTextViewTime(color: Int, time: String) {
        runOnUiThread {
            timerTextView.setTextColor(color)
            timerTextView.text = time
        }
    }

    override fun notifyDataInAdapter() {
        runOnUiThread {
            recyclerAdapter.notifyDataSetChanged()
        }
    }

    override fun movingBall(from: Int, to: Int) {
        val ball = View(this)
        ball.visibility = View.GONE
        ball.setBackgroundResource(R.drawable.ball)
        ball.layoutParams = FrameLayout.LayoutParams(50, 50)
        containerForBall.addView(ball)
        val xFrom = mapRecycler.getChildAt(from).x
        val yFrom = mapRecycler.getChildAt(from).y
        ball.x = xFrom
        ball.y = yFrom
        ball.visibility = View.VISIBLE
        val xTo = mapRecycler.getChildAt(to).x
        val yTo = mapRecycler.getChildAt(to).y


        ball.animate().x(xTo).y(yTo)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    ball.visibility = View.GONE
                    recyclerAdapter.notifyDataSetChanged()
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }
            })
            .start()
    }

    override fun showFinishGameResult(result: String) {
        AlertDialog.Builder(this)
            .setTitle("ИГРА ЗАВЕРШЕНА")
            .setMessage("")
            .create().show()
    }

    override fun showOneBot(position: Int) {
        when (position) {
            1 -> bootOne.setImageResource(R.drawable.interface_boot_active)
            2 -> bootTwo.setImageResource(R.drawable.interface_boot_active)
            3 -> bootThree.setImageResource(R.drawable.interface_boot_active)
        }
    }

    override fun hintOneBot(position: Int) {
        when (position) {
            1 -> bootOne.setImageResource(R.drawable.interface_boot_pasive)
            2 -> bootTwo.setImageResource(R.drawable.interface_boot_pasive)
            3 -> bootThree.setImageResource(R.drawable.interface_boot_pasive)
        }
    }

    override fun dismissGoalkeeperBot() {
        bootFour.setImageResource(R.drawable.icon_goalkeeper_black)
    }

    override fun showGoalkeeperBot() {
        bootFour.setImageResource(R.drawable.icon_goalkeeper_white)
    }

    override fun onPause() {
        presenter?.detachView()
        super.onPause()
    }
}