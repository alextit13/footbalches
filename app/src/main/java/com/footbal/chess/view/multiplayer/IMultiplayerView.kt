package com.footbal.chess.view.multiplayer

import com.footbal.chess.view.multiplayer.fragment.ChoseMultiplayerFragment

interface IMultiplayerView {
    fun initViews()
    fun initListeners()
    fun closeActivity()
    fun openChoseFragment(choseFragment: ChoseMultiplayerFragment)
    fun dismissAllFields()
    fun showAllFields()
    fun openTwoPlayerGame()
}