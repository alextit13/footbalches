package com.footbal.chess.view.two_players.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.footbal.chess.R
import com.footbal.chess.model.util.Player
import com.footbal.chess.view.game_web.adapter.IOnClickItemCallback
import kotlinx.android.synthetic.main.item_map.view.*

class TwoPlayerAdapter(
    var listCells: MutableList<Player>,
    private val context: Context,
    private val itemWidth: Int,
    private val itemHeight: Int,
    private val callback: IOnClickItemCallback
) : RecyclerView.Adapter<TwoPlayerAdapter.HolderAdapterGameMap>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderAdapterGameMap {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_map, parent, false)
        itemView.layoutParams = FrameLayout.LayoutParams(itemWidth, itemHeight)
        return HolderAdapterGameMap(itemView)
    }

    override fun getItemCount() = listCells.size

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun onBindViewHolder(holder: HolderAdapterGameMap, position: Int) {
        val player = listCells[position]
        holder.testPosition.text = position.toString()

        when (player.whatIs) {
            Player.WhatIs.CELL -> {
                holder.container.setBackgroundColor(Color.TRANSPARENT)
            }
            Player.WhatIs.PLAYER_MY -> {
                holder.item.setImageResource(R.drawable.player_blue)
            }
            Player.WhatIs.PLAYER_OTHER -> {
                holder.item.setImageResource(R.drawable.player_red)
            }
            Player.WhatIs.BALL -> {
                holder.item.setImageResource(R.drawable.ball)
            }
            Player.WhatIs.GOALKEEPER_OTHER -> {
                holder.item.setImageResource(R.drawable.goaly_red_full_size)
            }
            Player.WhatIs.GOALKEEPER_MY -> {
                holder.item.setImageResource(R.drawable.blu_drawn_norm_size)
            }
            Player.WhatIs.PLAYER_MY_WITH_BALL -> {
                holder.item.setImageResource(R.drawable.player_blue_with_ball)
            }
            Player.WhatIs.PLAYER_OTHER_WITH_BALL -> {
                holder.item.setImageResource(R.drawable.player_red_with_ball)
            }
        }

        holder.container.setOnClickListener { callback.onItemClick(position) }

        if (player.isSelection) {
            holder.itemView.animate().scaleY(1.1f).scaleX(1.1f).start()
        } else {
            holder.itemView.animate().scaleY(1f).scaleX(1f).start()
        }
    }

    class HolderAdapterGameMap(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val container: FrameLayout = itemView.fl_item_container
        val item: ImageView = itemView.item_map_tv
        val testPosition: TextView = itemView.text_view_test_position_on_map_item
    }
}