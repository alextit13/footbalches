package com.footbal.chess.view.game_web.adapter

interface IOnClickItemCallback {
    fun onItemClick(position: Int)
}