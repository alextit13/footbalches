package com.footbal.chess.view.game_web

import com.footbal.chess.model.util.Player

interface IGameWebActivity {
    fun initViews()
    fun initRecyclerView(dataForRecycler: MutableList<Player>, itemWidth: Int?, itemHeight: Int?)
    fun minimazePlayer(position: Int)
    fun maximazePlayer(position: Int)
    fun refreshAdapter(from: Int, to: Int)
    fun showToast(message: String)
    fun initListeners()
    fun refreshViews()
    fun notifyDataInAdapter()
    fun setTimeInTextViewTime(time: String)
    fun showFinishGameResult(result: String)
}