package com.footbal.chess.view.game_web

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.footbal.chess.R
import com.footbal.chess.model.util.Player
import com.footbal.chess.presenter.game_system.GameSystemPresenter.Companion.WIDTH_FIELD
import com.footbal.chess.presenter.game_web.GameWebPresenter
import com.footbal.chess.presenter.game_web.IGameWebPresenter
import com.footbal.chess.view.game_system.adapter.GameWebMapAdapter
import com.footbal.chess.view.game_web.adapter.IOnClickItemCallback
import kotlinx.android.synthetic.main.activity_game.*

class GameWebActivity : AppCompatActivity(), IGameWebActivity, IOnClickItemCallback {

    private var presenter: IGameWebPresenter? = null
    private lateinit var mapRecycler: RecyclerView
    private lateinit var container: RelativeLayout
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var recyclerAdapter: GameWebMapAdapter
    private lateinit var timerTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        attachPresenter()
    }

    private fun attachPresenter() {
        if (lastNonConfigurationInstance != null)
            presenter = lastNonConfigurationInstance as GameWebPresenter
        if (presenter == null) {
            presenter = GameWebPresenter()
        }
        presenter?.attachView(this)
    }

    override fun initViews() {
        mapRecycler = map_grid
        container = container_rl
        timerTextView = text_view_timer
    }

    override fun initListeners() {
        timerTextView.setOnClickListener { presenter?.onClickOnTimer() }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        presenter?.onHasFocuse(hasFocus, mapRecycler.width, mapRecycler.height)
    }

    override fun refreshViews() {
        runOnUiThread {
            onWindowFocusChanged(true)
        }
    }

    override fun initRecyclerView(dataForRecycler: MutableList<Player>, itemWidth: Int?, itemHeight: Int?) {
        layoutManager = GridLayoutManager(this, WIDTH_FIELD)
        recyclerAdapter = GameWebMapAdapter(dataForRecycler, this, itemWidth!!, itemHeight!!, this)
        mapRecycler.layoutManager = layoutManager
        mapRecycler.adapter = recyclerAdapter
        mapRecycler.recycledViewPool.setMaxRecycledViews(0, 0)
    }

    override fun refreshAdapter(from: Int, to: Int) {
        runOnUiThread {
            recyclerAdapter.notifyDataSetChanged()
        }
    }

    override fun onItemClick(position: Int) {
        presenter?.onItemClick(position)
    }

    override fun maximazePlayer(position: Int) {
        mapRecycler.getChildAt(position).animate().scaleX(1.2f).scaleY(1.2f).start()
    }

    override fun minimazePlayer(position: Int) {
        mapRecycler.getChildAt(position).animate().scaleX(1f).scaleY(1f).start()
    }

    override fun showToast(message: String) {
        runOnUiThread {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun setTimeInTextViewTime(time: String) {
        runOnUiThread { timerTextView.text = time }
    }

    override fun notifyDataInAdapter() {
        runOnUiThread {
            recyclerAdapter.notifyDataSetChanged()
        }
    }

    override fun showFinishGameResult(result: String) {
        AlertDialog.Builder(this)
            .setTitle("ИГРА ЗАВЕРШЕНА")
            .setMessage("")
            .create().show()
    }

    override fun onRetainCustomNonConfigurationInstance(): IGameWebPresenter? {
        return presenter
    }

    override fun onStop() {
        presenter?.detachView()
        super.onStop()
    }
}