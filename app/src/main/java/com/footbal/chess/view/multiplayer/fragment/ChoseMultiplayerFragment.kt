package com.footbal.chess.view.multiplayer.fragment

import android.app.Fragment
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListView
import android.widget.ProgressBar
import com.footbal.chess.R
import com.footbal.chess.model.util.Gamer
import com.footbal.chess.presenter.chose.ChosePresenter
import com.footbal.chess.presenter.chose.IChosePresenter
import com.footbal.chess.view.game_web.GameWebActivity
import com.footbal.chess.view.multiplayer.adapter.GamerAdapter
import kotlinx.android.synthetic.main.fragment_web.*

class ChoseMultiplayerFragment: Fragment(), IChoseView {

    private var presenter: IChosePresenter? = null

    private lateinit var listView: ListView
    private lateinit var refreshImageView: ImageView
    private lateinit var progress: ProgressBar

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_web, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = ChosePresenter()
        presenter?.attachView(this)
    }

    override fun initViews() {
        listView = list_view_players
        refreshImageView = image_view_refresh
        progress = progress_bar_choser
    }

    override fun initListeners() {
        refreshImageView.setOnClickListener { presenter?.onClickRefresh() }
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
        listView.visibility = View.GONE
        refreshImageView.visibility = View.GONE
    }

    override fun dismissProgress() {
        progress.visibility = View.GONE
        listView.visibility = View.VISIBLE
        refreshImageView.visibility = View.VISIBLE
    }

    override fun openActivityWebGame(gamer: Gamer) {
        val intent = Intent(activity, GameWebActivity::class.java)
        intent.putExtra(TAG_WEB_GAME, gamer.name)
        startActivity(intent)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun initAdapterWithGamers(list: MutableList<Gamer>) {
        val adapter = GamerAdapter(list, layoutInflater)
        listView.adapter = adapter
        listView.setOnItemClickListener { _, _, position, _ -> presenter?.onClickOnGamer(list[position]) }
    }

    override fun onStop() {
        super.onStop()
        presenter?.detachView()
    }

    companion object {
        const val TAG_WEB_GAME = "name_gamer_other"
    }
}