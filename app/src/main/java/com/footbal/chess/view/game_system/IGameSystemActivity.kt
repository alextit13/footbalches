package com.footbal.chess.view.game_system

import com.footbal.chess.model.util.Player

interface IGameSystemActivity {
    fun initViews()
    fun initRecyclerView(dataForRecycler: MutableList<Player>, itemWidth: Int?, itemHeight: Int?)
    fun minimazePlayer(position: Int)
    fun maximazePlayer(position: Int)
    fun refreshAdapter(from: Int, to: Int)
    fun showToast(message: String)
    fun initListeners()
    fun refreshViews()
    fun notifyDataInAdapter()
    fun setTimeInTextViewTime(time: String)
    fun showFinishGameResult(result: String)
    fun hintOneBot(position: Int)
    fun showOneBot(position: Int)
    fun movingBall(from: Int, to: Int)
}