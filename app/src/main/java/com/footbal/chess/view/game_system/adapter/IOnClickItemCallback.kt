package com.footbal.chess.view.game_system.adapter

interface IOnClickItemCallback {
    fun onItemClick(position: Int)
}